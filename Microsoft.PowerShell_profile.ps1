# Get latest container ID
function dl {
	docker ps -l -q
}

# Get container process
function dps {
	docker ps
}

# Get process including stopped container
function dpa {
	docker ps -a
}

# Get images
function di {
	docker images
}

# List all images
function dia {
	docker images -a
}

# Get container IP
function dip([string] $1)
{
	docker-machine ip $1
}

# Run deamonized container, e.g., $dkd base /bin/echo hello
function dkd {
	docker run -d -P
}

# Run interactive container, e.g., $dki base /bin/bash
function dki {
	docker run -i -t -P
}

# Execute interactive container, e.g., $dex base /bin/bash
function dex {
	docker exec -i -t
}

# Stop all containers
function dstop {
	docker stop $(docker ps -a -q)
}

# Remove a container
function drc([string] $1) {
	docker rm $1
}

# Remove all containers
function drca([string] $1) {
	docker rm $(docker ps -a -q)
}

# Stop and remove all containers
function dsrca {
	docker stop $(docker ps -a -q)
	if ($?) {
		docker rm $(docker ps -a -q)
	}
}

# Remove an image
function dri([string] $1) {
	docker rmi $1
}

# Remove all images
function dria([string] $1) {
	docker rmi $(docker images -q)
}

# Dockerfile build, e.g., $dbu tcnksm/test
function dbu([string] $1)
{
	docker build -t=$1 .
}

# Bash into running container
function dbash([string] $1) {
	docker exec -it $(docker ps -aqf "name=$1") bash
}

# Enter docker shell for particular machine
function dsh([string] $1) {
	docker-machine env --shell powershell $1 | Invoke-Expression
}

# Remove all volumes
function drv {
	docker volume rm $(docker volume ls -q)
}

# Run docker-compose.yml file
function dcu
{
	docker-compose up --remove-orphans
}

# Build and compose docker image
function dbc {
	docker-compose build --force-rm  --no-cache
	docker-compose up --force-recreate --remove-orphans
}

# List all volumes
function dvls {
	docker volume ls
}

# List all networks
function dnls {
	docker network ls
}

# Remove a particular network
function drn([string] $1)
{
	docker network rm $1
}

# Stop all containers, and remove all containers, images, and volumes
function dx {
	docker stop $(docker ps -a -q)
	docker rm $(docker ps -a -q)
	docker rmi $(docker images -q)
	docker volume rm $(docker volume ls -q)
}

# Export database associated wordpress docker image
function dedb {
	docker exec $(docker ps -lq) /bin/bash -c "wp db export /data/database_bk.sql --allow-root"
}

# Import database associated wordpress docker image
function didb {
	docker exec $(docker ps -lq) /bin/bash -c "wp db import /data/database.sql --allow-root"
}

function dcmd
{
	echo "dl: Get latest container ID";
	echo "ds: Get container process";
	echo "dpa: Get process included stop container";
	echo "di: Get images";
	echo "dia: Get all images";
	echo "dip {container}: Get container IP";
	echo "dkd: Run deamonized container, e.g., $dkd base /bin/echo hello";
	echo "dki: Run interactive container, e.g., $dki base /bin/bash";
	echo "dex: Execute interactive container, e.g., $dex base /bin/bash";
	echo "dstop: Stop all containers";
	echo "drc {container}: Remove a container";
	echo "drca: Remove all containers";
	echo "dsrca: Stop and Remove all containers";
	echo "dri {image/tag}: Remove an image";
	echo "dria: Remove all images";
	echo "dbu {image/tag}: Dockerfile build, e.g., $dbu tcnksm/test";
	echo "dbash {container}: Bash into running container";
	echo "dsh {container}: Bash into running container";
	echo "drv: Remove all volumes";
	echo "dcu: Run docker-compose.yml file";
	echo "dbc: Build and compose docker image";
	echo "dvls: List all volumes";
	echo "dnls: List all networks";
	echo "drn {network}: Remove a particular network";
	echo "dx: Stop all containers, and remove all containers, images, and volumes";
	echo "dedb: Export database associated wordpress docker image";
	echo "didb: Import database associated wordpress docker image";
}

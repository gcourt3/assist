# Assist Docker WordPress Environment

This is a Docker WordPress setup based on the 
[Visible Wordpress Starter Docker environment](https://github.com/visiblevc/wordpress-starter).  Basically, **Assist** 
creates a WordPress install on a [LAMP](https://en.wikipedia.org/wiki/LAMP_(software_bundle)) stack in a virtual 
machine.  By copying the .env.tmp file to .env and setting the appropriate environment variables there and possibly making some adjustments to the [docker-compose.yml](docker-compose.yml) file, 
**Assist** can reference _themes_ and _plugins_ located separately on your machine for inclusion in your WordPress 
install.  In our case, **Assist** must at least be used in conjunction with the 
[Torgersen Sage Wordpress Theme](https://git.it.vt.edu/rfentres/torgersen).  To do this, you must clone the 
**Torgersen** _theme_ separately to your local machine and set the path to that local instance in the appropriate 
[docker-compose.yml](docker-compose.yml) configuration file, as described below.  **Assist** also uses the 
[AblePlayer for WordPress](https://git.it.vt.edu/rfentres/ableplayer-wordpress) _plugin_, so, for that to work right, 
it must also be cloned locally and the path to it referenced as described below.

Finally, **Assist** is also populated with sample pages--at least one for each template--so that you can get a sense of 
what the theme does and can easily run accessibility tests on any changes you make to it.

<!-- ## Prerequisites

1. [Docker for Mac](https://www.docker.com/docker-mac).  The following info is for Mac, not Windows or Linux, though everything should work on those platforms with some modifications.  If you are not already familiar with Docker, you should review its [documentation](https://docs.docker.com/get-started/#setup) and consider taking the course, [Learning Docker](https://www.lynda.com/Docker-tutorials/Learning-Docker/485649-2.html), at Lynda.com.
2. [Node.js](http://nodejs.org/download/). Once you have installed Node, we recommend you update to the latest version of npm: `npm install -g npm@latest`.

### Set Up LAMP Stack in Docker and Provision Instance of WordPress

1. In .env, change `REPLACEMENT_URL` to whatever the URL is you will be accessing your development instance of WordPress from.

2. Also in [docker-compose.yml](docker-compose.yml), under ```volumes```, find the line that ends 
```:/app/wp-content/themes/torgersen``` and replace what comes before the colon there with the path to the 
**Torgersen** _theme_ on your local machine.  So, if you've cloned the Torgersen theme to 
`/Users/username/IdeaProjects/torgersen`, then, under volumes, it would read:
```
/Users/username/IdeaProjects/torgersen:/app/wp-content/themes/torgersen
```

3. Also in [docker-compose.yml](docker-compose.yml), under ```volumes```, find the line that ends 
```:/app/wp-content/plugins/ableplayer-wordpress``` and replace what comes before the colon there with the path to the 
**Ableplayer for Wordpress** _plugin_ on your local machine.  You will have had to check that out from 
[the repository](https://github.com/robfentress/ableplayer-wordpress.git) first.    So, if you've cloned the Torgersen 
theme to `/Users/username/IdeaProjects/ableplayer-wordpress`, then, under `volumes`, it would read:
```
/Users/username/IdeaProjects/ableplayer-wordpress:/app/wp-content/plugins/ableplayer-wordpress
```

4. Build, compose, start and provision an instance of WordPress.  Making sure you've already started the Docker 
machine and connected to the Docker shell, then, from the same directory where docker-compose.yml is, type the 
following at the command line: 
> `docker-compose build --force-rm  --no-cache; docker-compose up --force-recreate --remove-orphans` or `dbc` if 
using [shell script](#shell-script]) aliases and functions

**NOTE:** If you pull from the original repository again, you may overwrite the changes you have made to 
[docker-compose.yml](docker-compose.yml).

### Starting Up Docker Machine and the Wordpress Environment

When you restart your computer or stop the running docker machine, you will need to do the following to start the 
Docker virtual machine again.  From the command line, enter: 

```
docker-machine start default
```

If you close a terminal window, when you open a new one, you will need to connect to the Docker shell again, by typing 
from the command line: 

> `eval "$(docker-machine env default)"` or `dsh default` if using [shell script](#shell-script]) aliases and functions.

If you stop the web and database server containers, to restart them, at the command line, type the following from the 
same directory where docker-compose.yml is: 

> `docker-compose up --remove-orphans` or `dcu` if using [shell script](#shell-script]) script aliases and functions. -->

## Theme development

These instructions only handle setting up the Assist Docker WordPress Environment.  To actually use and modify the 
Torgersen theme, you will need to set that up separately, following the instructions on its 
[README](https://git.it.vt.edu/rfentres/torgersen).


## Useful Docker commands
* List all top-level Docker images by executing `docker images`.
* List all Docker images, including intermediate images, by executing `docker images -a`.
* Remove a specified Docker image by executing `docker rmi`.
* Remove _all_ Docker images by executing `docker rmi $(docker images -q)`.
* List the last run Docker container process by executing `docker ps -l -q`.
* List all _running_ Docker container processes by executing `docker ps`.
* List _all_ Docker container processes by executing `docker ps -a`.
* Stop a specified Docker container by executing `docker stop`.
* Stop _all_ running Docker containers by executing `docker stop $(docker ps -a -q)`.
* Remove a specified Docker container by executing `docker rm`.
* Remove _all_ Docker containers by executing `docker rm $(docker ps -a -q)`.
* Stop and removes all Docker containers by executing `docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)`.
* List _all_ Docker networks by executing `docker network ls`.
* Remove a specified Docker network by executing `docker network rm`.
* Remove _all_ Docker networks by executing `docker network rm $(docker ps -q)`.
* List _all_ Docker volumes by executing `docker volume ls`.
* Remove a specified Docker volume by executing `docker volume rm`.
* Remove a specified Docker volume by executing `docker volume rm $(docker volume ls -q)`.
* Stop _all_ running Docker containers and then remove _all_ containers, images, and volumes by executing `docker stop $(docker ps -a -q) ; docker rm $(docker ps -a -q) ; docker rmi $(docker images -q) ; docker volume rm $(docker volume ls -q) ;`.
* Run daemonized Docker container by executing `docker run -d -P`.
* Run interactive Docker container by executing `docker run -i -t -P`.
* Execute a command in an interactive Docker container by executing `docker exec -i -t`.


## More Info on Visible Wordpress Starter

Learn more about the [Visible Wordpress Starter Docker environment](https://github.com/visiblevc/wordpress-starter) 
that was the starting point for **Assist**.

# ------------------------------------
# Docker alias and function
# ------------------------------------

# Enter docker shell for particular machine
function dsh { eval "$(docker-machine env $1)"; } 

# Get container IP
function dip { docker-machine ip $1; }

# Dockerfile build, e.g., $dbu tcnksm/test
function dbu { docker build -t=$1 .; }

# Build and compose docker image
alias dbc="docker-compose build --force-rm  --no-cache ; docker-compose up --force-recreate --remove-orphans"

# Run docker-compose.yml file
function dcu { docker-compose up --remove-orphans; }

# Get images
alias di="docker images"

# List all images
alias dia="docker images -a"

# Remove all images
function dri { docker rmi $(docker images -q); }

# Get latest container ID
alias dl="docker ps -l -q"

# Bash into running container
function dbash { docker exec -it $(docker ps -aqf "name=$1") bash; }

# Get container process
alias dps="docker ps"

# Get process included stop container
alias dpa="docker ps -a"

# Run deamonized container, e.g., $dkd base /bin/echo hello
alias dkd="docker run -d -P"

# Run interactive container, e.g., $dki base /bin/bash
alias dki="docker run -i -t -P"

# Execute interactive container, e.g., $dex base /bin/bash
alias dex="docker exec -i -t"

# Stop all containers
function dstop { docker stop $(docker ps -a -q); }

# Remove all containers
function drm { docker rm $(docker ps -a -q); }

# Stop and Remove all containers
alias drmf='docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)'

# List all volumes
alias dvls="docker volume ls"

# Remove all volumes
function drv { docker volume rm $(docker volume ls -q); }

# List all networks
alias dnls="docker network ls"

# Remove a particular network
function drn { docker network rm $1 ; }

# Stop all containers, and remove all containers, images, and volumes
function dx { docker stop $(docker ps -a -q) ; docker rm $(docker ps -a -q) ; docker rmi $(docker images -q) ; docker volume rm $(docker volume ls -q) ; }

# Export database associated wordpress docker image
function dedb { docker exec $(docker ps -lq) /bin/bash -c "wp db export /data/database_bk.sql --allow-root"; }

# Import database associated wordpress docker image
function didb { docker exec $(docker ps -lq) /bin/bash -c "wp db import /data/database.sql --allow-root"; }

function dcmd {
	echo "dl: Get latest container ID";
	echo "ds: Get container process";
	echo "dpa: Get process included stop container";
	echo "di: Get images";
	echo "dia: Get all images";
	echo "dip {container}: Get container IP";
	echo "dkd: Run deamonized container, e.g., $dkd base /bin/echo hello";
	echo "dki: Run interactive container, e.g., $dki base /bin/bash";
	echo "dex: Execute interactive container, e.g., $dex base /bin/bash";
	echo "dstop: Stop all containers";
	echo "drm: Remove all containers";
	echo "drmf: Stop and Remove all containers";
	echo "dri: Remove all images";
	echo "dbu {image/tag}: Dockerfile build, e.g., $dbu tcnksm/test";
	echo "dbash {container}: Bash into running container";
	echo "dsh {container}: Bash into running container";
	echo "drv: Remove all volumes";
	echo "dcu: Run docker-compose.yml file";
	echo "dbc: Build and compose docker image";
	echo "dvls: List all volumes";
	echo "dnls: List all networks";
	echo "drn {network}: Remove a particular network";
	echo "dx: Stop all containers, and remove all containers, images, and volumes";
	echo "dedb: Export database associated wordpress docker image";
	echo "didb: Import database associated wordpress docker image";
}
